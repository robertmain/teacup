/*!
 * @copyright Elemental Software 2015 - A teacup is filled with sugar whilst I play unfitting music.
 * @version v0.0.2
 * @link http://bitbucket.com/elementalsoftware/pillars
 * @license MIT License, http://www.opensource.org/licenses/MIT
 * @revision fab3b88
*/
!function(){"use strict";angular.module("pillars",["pillars.controllers","pillars.filters","ui.router"]).config(["$stateProvider","$urlRouterProvider","$httpProvider","$provide","$locationProvider",function(r,t){r.state("app",{controller:"MainCtrl",templateUrl:"views/home.html",url:"/"}).state("app.content",{}).state("404",{templateUrl:"views/404.html",url:"/404"}),t.when("","/").otherwise("/404")}]).run(["$state",function(r){r.go("app")}])}(),function(){"use strict";angular.module("pillars.controllers",[]).controller("MainCtrl",function(){}).controller("SidebarCtrl",[function(){}])}(),function(){"use strict";angular.module("pillars.filters",[]).filter("reverse",function(){return function(r){return r.split("").reverse().join("")}}).filter("timeago",function(){return function(r){return moment(r).fromNow()}}).filter("stripTags",function(){return function(r){return String(r).replace(/<[^>]+>/gm,"")}})}();